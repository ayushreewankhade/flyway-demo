package com.example.flywaydemo.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "people")
public class Person {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "role")
    private String role;

    @Column(name = "experience")
    private Integer experience;

    public Person() { }

    public Person(Long id, String name, String role, Integer experience) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.experience = experience;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getRole() {
        return this.role;
    }

    public Integer getExperience() {
        return this.experience;
    }
}
